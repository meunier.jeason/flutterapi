import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutterapi/model/post.dart';
import 'package:http/http.dart';

class JsonParsingMap extends StatefulWidget {
  @override
  _JsonParsingMapState createState() => _JsonParsingMapState();
}

class _JsonParsingMapState extends State<JsonParsingMap> {
  Future<PostList> data;

  @override
  void initState(){
    super.initState();
    String url = "https://jsonplaceholder.typicode.com/posts";
    Network network = Network(url);
    data = network.loadPosts();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Parsing JSON"),
      ),
      body: Center(
        child: Container(
          child: FutureBuilder(
            future: data,
            builder: (context,AsyncSnapshot<PostList> snapshot){
              List<Post> allPosts;
              if(snapshot.hasData){
                allPosts = snapshot.data.posts;
                return createListView(context,allPosts);
              }
              return CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }

  Widget createListView(BuildContext context,List<Post> data) {
    return Container(
      child: ListView.builder(
        itemCount: data.length,
        itemBuilder: (context,int index){
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Divider(height: 5.0,),
              ListTile(
                title: Text("${data[index].title}"),
                subtitle: Text("${data[index].body}"),
                leading: Column(
                  children: [
                    CircleAvatar(
                      backgroundColor: Colors.black26,
                      radius: 23,
                      child: Text("${data[index].id}"),
                    )
                  ],
                ),
              )
            ],
          );
        },
      ),
    );
  }
}

class Network{
  final String url;
  Network(this.url);

  Future<PostList> loadPosts() async{
    print("$url");
    Response response = await get(Uri.encodeFull(url));

    if(response.statusCode == 200){
      print(response.body);
      return PostList.fromJson(json.decode(response.body));
    }else{
      throw Exception("Failed load JSON");
    }
  }
}